<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedules".
 *
 * @property string $id Первичный ключ
 * @property string $start_station_id Станция отправления
 * @property string $end_station_id Станция прибытия
 * @property string $carrier_id Перевозчик
 * @property string $start_time Время отправления
 * @property string $end_time Время прибытия
 * @property double $price Цена проезда
 * @property int $days График движения
 *
 * @property Carriers $carrier
 * @property Stations $startStation
 * @property Stations $endStation
 */
class Schedules extends \yii\db\ActiveRecord
{

    /**
     * @var array
     */
    public $days_schedule = [
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суббота',
        7 => 'Воскресенье',
    ];

    public $days;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedules';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_station_id', 'end_station_id', 'carrier_id', 'start_time', 'end_time', 'price', 'days'], 'required'],
            [['start_station_id', 'end_station_id', 'carrier_id'], 'integer'],
            [['start_time', 'end_time'], 'safe'],
            [['price'], 'number'],
            [['carrier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Carriers::className(), 'targetAttribute' => ['carrier_id' => 'id']],
            [['start_station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Stations::className(), 'targetAttribute' => ['start_station_id' => 'id']],
            [['end_station_id'], 'exist', 'skipOnError' => true, 'targetClass' => Stations::className(), 'targetAttribute' => ['end_station_id' => 'id']],
/*
            ['days', 'filter', 'filter' => function ($value)
            {
                $int_value = 0;
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $int_value = $int_value | bindec('1' . str_repeat('0', $v - 1));
                    }
                }

                return $int_value;
            }],
            */
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'Первичный ключ',
            'start_station_id' => 'Станция отправления',
            'end_station_id'   => 'Станция прибытия',
            'carrier_id'       => 'Перевозчик',
            'start_time'       => 'Время отправления',
            'end_time'         => 'Время прибытия',
            'price'            => 'Цена проезда',
            'days'             => 'График движения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrier()
    {
        return $this->hasOne(Carriers::className(), ['id' => 'carrier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'start_station_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'end_station_id']);
    }




    
    public function getDaysNumbers()
    {
        $daysNumbers = [];
        $bin = decbin($this->days);
        $rev = strrev($bin);
        $i = 1;
        foreach(str_split($rev) as $v)
        {
            if (intval($v))
            {
                $daysNumbers[] = $i;
            }
            $i++;
        }

        return $daysNumbers;
    }


    public function getMultipleOptopnsSelected()
    {
        $ret = [];
        foreach ($this->getDaysNumbers() as $v) {
            $ret[$v] = ['selected' => 'selected'];
        } 
        return $ret;
    }

    public function getDaysNames()
    {
        $ret = [];
        foreach ($this->getDaysNumbers() as $v) {
            $ret[] = $this->days_schedule[$v];
        } 
        return implode(', ',$ret);
    }
}
